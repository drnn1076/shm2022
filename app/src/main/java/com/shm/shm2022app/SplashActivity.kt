package com.shm.shm2022app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import java.util.Timer
import kotlin.concurrent.timerTask


class SplashActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        mainActivity()

    }

    private fun mainActivity(){
        if(!isDestroyed) {
            // especificar el intent para ir a la siguiente actividad
            val intent = Intent(this, MainActivity::class.java)
            // schedule el timer con timerTask
            val tmTask = timerTask {
                if (!isDestroyed) {
                    startActivity(intent)
                    finish()
                }
            }
            // iniciar el timer
            val timer = Timer()
            // indicar el delay en milisegundos
            timer.schedule(tmTask, 2000)
        }

    }
}