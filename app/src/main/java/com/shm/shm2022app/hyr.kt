package com.shm.shm2022app

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [hyr.newInstance] factory method to
 * create an instance of this fragment.
 */
class hyr : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var button: Button
    private lateinit var button1: Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_hyr, container, false)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment hyr.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            hyr().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val rollButton1: Button = view.findViewById(R.id.mapsH1)
        rollButton1.setOnClickListener {replaceFragment(mapas())}
        val rollButton2: Button = view.findViewById(R.id.mapsH2)
        rollButton2.setOnClickListener {replaceFragment(mapas())}
        val rollButton3: Button = view.findViewById(R.id.mapsH3)
        rollButton3.setOnClickListener {replaceFragment(mapas())}
        val rollButton4: Button = view.findViewById(R.id.mapsH4)
        rollButton4.setOnClickListener {replaceFragment(mapas())}
        val rollButton5: Button = view.findViewById(R.id.mapsH5)
        rollButton5.setOnClickListener {replaceFragment(mapas())}

        button = view.findViewById(R.id.moreInfo)
        button.setOnClickListener {
            val url = "https://bit.ly/SHM2022HOTEL"
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            startActivity(i)
        }


        super.onViewCreated(view, savedInstanceState)
    }

    private fun replaceFragment(fragment: Fragment){
        val fragmentManager = parentFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, fragment)
        fragmentTransaction.commit()

    }

}