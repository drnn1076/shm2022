package com.shm.shm2022app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.shm.shm2022app.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        replaceFragment(home())
        binding.bottomNavigation.setOnItemSelectedListener {
            when(it.itemId){
                R.id.home -> replaceFragment(home())
                R.id.programa -> replaceFragment(programa())
                R.id.ponentes -> replaceFragment(ponentes())
                R.id.hyr -> replaceFragment(hyr())
                R.id.mapas -> replaceFragment(mapas())
                else -> {
                }
            }
            true
            }

    }

    override fun onBackPressed() {
        if (supportFragmentManager.popBackStackImmediate()){
            super.onBackPressed();
        }

    }



    private fun replaceFragment(fragment: Fragment){
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragment_container, fragment)
        fragmentTransaction.commit()

    }
}