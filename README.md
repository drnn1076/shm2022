# SHM2022

SHM2022 es un app para eventos académicos, conferencias, simposios, congresos, festivales para dispositivos android. 

## Estatus del proyecto
Este proyecto ya está terminado y este git es para documentación y uso interno en la Universidad de Antioquia

## App en google play
https://play.google.com/store/apps/details?id=com.shm.shm2022app


## Autores
Ricardo Cedeño Montaña, Daniela Escobar Gaviria, Yuli Paola Vargas Rodriguez
Universidad de Antioquia. Medellín Colombia, 2022

## Derechos
Derechos reservados. Universidad de Antioquia, Medellín, Colombia. 2024
